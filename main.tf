terraform {
  backend "http" {}
  required_providers {
    aws = {
      version = ">= 3.50.0"
      source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  profile = var.profile
  region  = var.aws_region
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_ami" "debian_10" {
  most_recent = true
  owners      = ["136693071363"]

  filter {
    name   = "name"
    values = ["debian-10-amd64-*"]
  }
}

resource "aws_key_pair" "my_key" {
  key_name   = "MyKeyPair"
  public_key = file(var.public_key)
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.7.0"

  name = "desafio-vpc"
  cidr = var.vpc_cidr_block

  azs             = data.aws_availability_zones.available.names
  private_subnets = slice(var.private_subnet_cidr_blocks, 0, var.private_subnets_per_vpc)
  public_subnets  = slice(var.public_subnet_cidr_blocks, 0, var.public_subnets_per_vpc)

  enable_nat_gateway = true
  enable_vpn_gateway = false
  tags = {
    Name = "Desafio-VPC"
  }

}

resource "aws_security_group" "lb_sg" {
  description = "Security group for lb-server with HTTP ports open within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "LB-SG"
  }
}

resource "aws_security_group" "app_sg" {
  description = "Security group for app-server with HTTP ports open within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "Acesso ao stats somente da vpc"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "APP-SG"
  }
}

resource "aws_security_group" "sg_vpn" {
  description = "Acesso a VPN"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 500
    to_port     = 500
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 1194
    to_port     = 1194
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 4500
    to_port     = 4500
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "SG-VPN"
  }
}

resource "aws_security_group" "ssh_sg" {
  vpc_id      = module.vpc.vpc_id
  description = "Acesso ssh a instancia"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }
  tags = {
    Name = "SSH-SG"
  }
}


resource "aws_security_group" "db_sg" {
  description = "Security group for DB-server"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "DB-SG"
  }
}

resource "aws_db_subnet_group" "private" {
  subnet_ids = [module.vpc.private_subnets.0, module.vpc.private_subnets.1]
}

//    INSTANCIAS
resource "aws_db_instance" "dbpostgresql" {
  name              = "dbpostgresql"
  identifier        = "dbpostgresql"
  allocated_storage = 5
  engine            = "postgres"
  engine_version    = "13.1"
  instance_class    = "db.t3.micro"
  username          = var.db_username
  password          = var.db_password

  vpc_security_group_ids = [aws_security_group.db_sg.id]
  db_subnet_group_name   = aws_db_subnet_group.private.name

  skip_final_snapshot = true

  tags = {
    Name = "RDS-DATABASE"
  }
}

resource "aws_instance" "haproxy" {
  ami                    = data.aws_ami.debian_10.id
  instance_type          = var.ec2_instance_type
  key_name               = aws_key_pair.my_key.id
  vpc_security_group_ids = [aws_security_group.lb_sg.id, aws_security_group.ssh_sg.id]
  subnet_id              = module.vpc.public_subnets[0]

  tags = {
    Name = "HAPROXY"
  }
}

resource "aws_instance" "vpn_ipsec" {
  ami                    = "ami-080471172a731411b"
  instance_type          = var.ec2_instance_type
  key_name               = aws_key_pair.my_key.id
  vpc_security_group_ids = [aws_security_group.sg_vpn.id, aws_security_group.ssh_sg.id]
  subnet_id              = module.vpc.public_subnets[1]

  provisioner "file" {
    source      = "vpn.sh"
    destination = "/tmp/vpn.sh"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(var.private_key)
      host        = self.public_ip
    }
  }
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(var.private_key)
      host        = self.public_ip
    }
    inline = [
      "sudo apt-get update",
      "chmod +x /tmp/vpn.sh",
      "sudo /tmp/vpn.sh"
    ]
  }

  tags = {
    Name = "VPN-IPSEC"
  }
}

resource "aws_instance" "metabase" {
  ami                    = data.aws_ami.debian_10.id
  instance_type          = var.ec2_instance_type
  key_name               = aws_key_pair.my_key.id
  vpc_security_group_ids = [aws_security_group.app_sg.id, aws_security_group.ssh_sg.id]
  subnet_id              = module.vpc.private_subnets[2]

  tags = {
    Name = "METABASE"
  }
}


