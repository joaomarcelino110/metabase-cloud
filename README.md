# metabase-cloud
Para levantar a aplicação do metabase foi construído uma insfraestrtura em terraform para implantar na AWS consistindo dos seguintes itens:

- 1 VPC na região us-west-2 com o seguinte
- 2 subnets publicas
- 3 subnets privadas
- 1 instancia de balanceador
- 1 instancia de vpn
- 1 instancia metabase
- 1 instancia postgres rds

O balanceador e a vpn ficaram em redes publicas enquanto o matabase e o banco em redes privadas e distintas.

Cada instância teve um ou mais security_groups atrelados para liberar acesso somente nas portas que vai necessitar

O Terraform cria toda a infraestrutura e provisiona as configurações da vpn, sua saída é atrelada a templates que serão arquivos de configuração para o ansible, haproxy e docker-compose.

Para executar o ansible, necessita apenas dos dados de configuração da vpn hardcodado (para efeitos de visualização) no script da vpn e do ip publico da vpn. 

Depois de conectar é somente executar o ansible que ele irá configurar as demais instancias.

Para visualizar o metabase pelo navegador deve-se adicionar uma entrada no arquivo /etc/hosts/ com a seguinte conteúdo

```<ip_publico_balanceador>     metabase.desafio.aws```

O `ip_publico_do_balanceador` será exibido juntamente com o ip publico da vpn ao final da execução do terraform.
