variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "us-west-2"
}

variable "profile" {
  description = "Profile Selected"
  type        = string
  default     = "terraform_adm_user"
}

variable "vpc_cidr_block" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_subnets_per_vpc" {
  description = "Number of public subnets."
  type        = number
  default     = 2
}

variable "private_subnets_per_vpc" {
  description = "Number of private subnets."
  type        = number
  default     = 3
}

variable "public_subnet_cidr_blocks" {
  description = "Available cidr blocks for public subnets."
  type        = list(string)
  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
  ]
}

variable "private_subnet_cidr_blocks" {
  description = "Available cidr blocks for private subnets."
  type        = list(string)
  default = [
    "10.0.101.0/24",
    "10.0.102.0/24",
    "10.0.103.0/24",
  ]
}


variable "ip_ec2_aytoscalling" {
  description = "Available cidr blocks for private subnets."
  type        = list(string)
  default = [
    "10.0.101.10/24",
    "10.0.102.11/24",
    "10.0.102.12/24",
    "10.0.102.13/24",
    "10.0.102.14/24",

  ]
}

variable "public_key" {
  sensitive = true
}

variable "private_key" {
  sensitive = true
}


variable "ec2_instance_type" {
  description = "AWS EC2 instance type."
  type        = string
  default     = "t3.micro"
}

variable "db_username" {
  description = "Database administrator username."
  type        = string
  sensitive   = true
}

variable "db_password" {
  description = "Database administrator password."
  type        = string
  sensitive   = true
}