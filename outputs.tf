output "publicIP_HAProxy" {
  description = "IP publico das instancias da rede publica"
  value       = aws_instance.haproxy.public_ip
}

output "publicIP_VPN_IPsec" {
  description = "IP publico das instancias da rede pblica"
  value       = aws_instance.vpn_ipsec.public_ip
}

resource "local_file" "AnsibleInvetory" {
  content = templatefile("inventory.tmpl", {
    haproxy-priv  = aws_instance.haproxy.private_ip,
    metabase-priv = aws_instance.metabase.private_ip
  })
  filename = "inventory.yml"
}

resource "local_file" "HaproxyEntry" {
  content = templatefile("./roles/balancer/templates/template-haproxy.cfg", {
    metabase-priv = aws_instance.metabase.private_ip
  })
  filename = "./roles/balancer/templates/haproxy.cfg"
}

resource "local_file" "Metabase" {
  content = templatefile("./roles/metabase/templates/compose-template", {
    postgres_address = aws_db_instance.dbpostgresql.address
    db_username      = aws_db_instance.dbpostgresql.username
    db_password      = aws_db_instance.dbpostgresql.password
    db_port          = aws_db_instance.dbpostgresql.port
    db_username      = aws_db_instance.dbpostgresql.name
  })
  filename = "./roles/metabase/templates/docker-compose.yml"
}